import React, { Component } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import WeatherWidget from "./components/WeatherWidget";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class App extends Component {
  render() {
    return (
      <Container>
        <Row>
          <Col>
            <WeatherWidget></WeatherWidget>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
