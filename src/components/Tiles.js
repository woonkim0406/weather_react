import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Image from "react-bootstrap/Image";
import React, { useState, useEffect } from "react";
import Row from "react-bootstrap/Row";
import axios from "axios";

export default function Tiles(props) {
  const [count, setCount] = useState(0);
  const [weatherData, setWeatherData] = useState({
    main: "",
    temp: "",
    icon: "",
  });

  async function callApi() {
    let resp;
    try {
      resp = await axios.get(
        `http://api.openweathermap.org/data/2.5/weather?q=${props.city}&appid=${process.env.REACT_APP_API_KEY}`
      );
    } catch (e) {
      let err;
      if (e.message == "Request failed with status code 401") {
        err = "401";
      }

      props.handleInvalidCity();
      if (err == "401") {
        alert("Not authorized");
      } else {
        alert("Invalid city");
      }
      return;
    }

    let _temp = resp.data.main.temp - 273.15;
    _temp = _temp.toPrecision(2);
    let _weather = {
      main: resp.data.weather[0].main,
      temp: _temp,
      icon: resp.data.weather[0].icon,
    };
    setWeatherData(_weather);
  }
  let interval_obj;
  useEffect(() => {
    callApi();
    if (props.time <= 0) {
      return;
    }
    interval_obj = setInterval(() => {
      setCount((prev) => prev + 1);
      callApi();
    }, props.time * 1000);
    return function cleanup() {
      clearInterval(interval_obj);
    };
  }, []);

  return (
    <Container>
      <Row>
        <Col className="p-0">{props.city} </Col>
      </Row>

      {weatherData.icon && (
        <Row>
          <Col className="p-0">
            <Image
              src={
                "http://openweathermap.org/img/wn/" +
                weatherData.icon +
                "@2x.png"
              }
              thumbnail
            />
          </Col>
        </Row>
      )}
      <Row className="justify-content-between">
        <Col xs={12} sm={6} className="p-0">{weatherData.temp + "°C"}</Col>
        <Col xs={12} sm={6} className="p-0">{weatherData.main}</Col>
      </Row>
      <Row className="justify-content-between">
        <Col className="p-0">API count: {count}</Col>
      </Row>
    </Container>
  );
}
