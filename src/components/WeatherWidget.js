import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import React, { useState, useEffect } from "react";
import Row from "react-bootstrap/Row";
import Tiles from "./Tiles";
import _ from "lodash";

export default function WeatherWidget() {
  const [city, setCity] = useState("Kuala Lumpur");
  const [counter, setCounter] = useState(0);
  const [interval, setInterval] = useState(0);
  const [weatherList, setWeatherList] = useState(JSON.stringify([]));
  const [weatherList_D, setWeatherList_D] = useState([]);

  useEffect(() => {
    let wi = [];
    for (let i = 0; i < 1; i++) {
      wi.push({
        interval: 0,
        city: "Kuala Lumpur",
        id: i,
      });
      setCounter(1);
    }
    setWeatherList(JSON.stringify(wi));
  }, []);

  useEffect(() => {
    setWeatherList_D(JSON.parse(weatherList));
  }, [weatherList]);

  function handleInvalidCity() {
    let wl = JSON.parse(weatherList);
    wl.pop();
    setWeatherList(JSON.stringify(wl));
  }

  return (
    <Container>
      <Row className="justify-content-between">
        {weatherList_D.map((o) => {
          return (
            <Col
              xs={3}
              lg={2}
              key={o.id}
              className={"tiles "}
              onClick={() => {
                let wl = JSON.parse(weatherList);
                let temp = wl.filter((weather) => {
                  return weather.id !== o.id;
                });
                setWeatherList(JSON.stringify(temp));
              }}
            >
              <Tiles
                time={o.interval}
                id={o.id}
                city={o.city}
                handleInvalidCity={handleInvalidCity}
              ></Tiles>
            </Col>
          );
        })}
      </Row>

      <Row>
      
        <Col xs={false} sm={true}></Col>
        <Col>
          <Form>
            <Form.Group>
              <Form.Label>City</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter city"
                defaultValue={city}
                onChange={(e) => {
                  setCity(e.target.value);
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Interval</Form.Label>
              <Form.Control
                type="text"
                placeholder="In seconds"
                defaultValue={interval}
                onChange={(e) => {
                  setInterval(e.target.value);
                }}
              />
            </Form.Group>

            <Button
              variant="primary"
              onClick={() => {
                let _interval = _.toNumber(interval);
                if (_.isNaN(_interval) || _interval < 0) {
                  alert("Interval must be number");
                  return;
                }
                let wl = JSON.parse(weatherList);
                let wi = {
                  interval: interval,
                  city: _.startCase(_.toLower(city)),

                  id: counter,
                };
                setCounter((prev) => prev + 1);
                wl.push(wi);
                setWeatherList(JSON.stringify(wl));
              }}
            >
              Submit
            </Button>
          </Form>
        </Col>
        <Col xs={false}sm={true}></Col>
      </Row>
    </Container>
  );
}
