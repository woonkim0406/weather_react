**Setup process:**
Development mode
-
1. Pull the code.
2. Key in the API Key in .env
3. Run `npm install`

Production mode
-
4. Pull the code.
5. Key in the API Key in .env
6. Run
	`npm run build`
	`npm install -g serve`
	`serve -s build`

Usage
-
1. Key in city name in text field provided and it's running interval indicated in 'seconds', click 'submit' to add the city's weather's tiles.
2. Hover and click the tiles to remove it.
 